from django.http import HttpResponse
from django.shortcuts import redirect


def home(request):
    if request.session.has_key('user'):
        if request.session['admin']:
            return HttpResponse("Hello, "+request.session['user']+" you are an admin.")
        else:
            return HttpResponse("Hello, "+request.session['user']+" you aren't an admin.")
    else:
        return redirect('login')
        
    return HttpResponse("Hello, world. You're at the index.")

def login(request):
    if request.POST.has_key('name'):
        request.session['admin'] = True
        request.session['user'] = request.POST['name']
        return redirect('home')
    else:
        return HttpResponse('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">'+\
                            '<html>'+\
                            '<head>'+\
                            '<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">'+\
                            '<title>Login</title>'+\
                            '</head>'+\
                            '<body>'+\
                            '<form action="login" method="POST">'+\
                            '<input type="text" name="name">'+\
                            '<input type="submit">'+\
                            '</form>'+\
                            '</body>'\
                            )