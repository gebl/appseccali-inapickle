from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'inapickle.views.home', name='home'),
    url(r'^login$', 'inapickle.views.login', name='login'),
    url(r'^admin/', include(admin.site.urls)),
)
